import React, { Component } from "react";
import Map from "./Map";
import Header from "./Header";
import { Slide, TabPanel } from "@material-ui/core";
import Sidebar from "./Sidebar";
import { Container } from "reactstrap";
import { properties } from "./Constant";
import axios from "axios";
import classnames from "classnames";
import {
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,
  Card,
  Button,
  CardTitle,
  CardText,
  Row,
  Col,
} from "reactstrap";

export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      activeTab: "1",
      data: [],
      totalTruck: 0,
      runningTruck: [],
      stoppedTRuck: [],
      idleTruck: [],
      errorTruck: [],
      recogniser: "all",
    };
  }

  // componentWillMount = () => {
  //   this.setState({
  //     data: [],
  //   });
  // };
  toggle = (tab) => {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab,
      });
    }
  };

  componentDidMount = (data) => {
    console.log("shahid calling com");
    let running = [];
    let stopped = [];
    let Total = [];
    let idle = [];
    let error = [];

    axios
      .get(properties.url)
      .then((response) => {
        if (response.status === 200) {
          Total = response.data.data;
          console.log("dataaaaaaaaaaa", response.data.data);
          for (let i = 0; i < response.data.data.length; i++) {
            if (
              response.data.data[i].lastRunningState.truckRunningState === 0 &&
              response.data.data[i].lastWaypoint.ignitionOn === false
            ) {
              // console.log("shahid==> stopped", response.data.data[i]);
              stopped.push(response.data.data[i]);
            } else if (
              response.data.data[i].lastRunningState.truckRunningState === 1
            ) {
              running.push(response.data.data[i]);
            } else if (
              response.data.data[i].lastRunningState.truckRunningState === 0 &&
              response.data.data[i].lastWaypoint.ignitionOn === true
            ) {
              idle.push(response.data.data[i]);
            } else if (response.data.data[i].breakdown === true) {
              error.push(response.data.data[i]);
            } else {
              console.log(
                "dont know how to recognise idle truck and error truck "
              );
            }
          }
          this.setState(
            {
              data: response.data.data,
              totalTruck: Total,
              runningTruck: running,
              stoppedTRuck: stopped,
              idleTruck: idle,
              errorTruck: error,
            },
            () => {
              console.log("totalllll", this.state);
            }
          );
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  stopedHandler = () => {
    this.setState(
      {
        data: this.state.stoppedTRuck,
        activeTab: "3",
      },
      () => console.log("stopped", this.state.recogniser)
    );
  };
  totalHandler = () => {
    console.log("shahid total");
    this.setState({
      data: this.state.totalTruck,
      recogniser: "all",
      activeTab: "1",
    });
  };

  runningHandler = () => {
    console.log("calling from   running");
    this.setState({
      data: this.state.runningTruck,
      recogniser: "running",
      activeTab: "2",
    });
  };
  idelHandler = () => {
    console.log("idellll", this.state.idleTruck);
    this.setState({
      activeTab: "4",
      data: this.state.idleTruck,
    });
  };
  errorHandler = () => {
    this.setState({
      activeTab: "5",
      data: this.state.errorTruck,
    });
  };

  onSelect = (passeddata) => {
    console.log("calling", passeddata);
    let data = this.state.data;
    data.push(passeddata);
    // this.setState({
    //   data: data,
    // });
  };

  dropdownchange = (data) => {
    console.log("dropdown down change calling", data);
    this.setState({
      data: data,
    });
  };

  render() {
    console.log("renderingrrr");
    return (
      <Container
        fluid
        style={{
          width: "100%",
          maxWidth: "none",
          paddingLeft: 0,
          paddingRight: 0,
        }}
      >
        <Header
          data={this.state.data}
          total={this.state.totalTruck}
          Stoppped={this.state.stoppedTRuck}
          running={this.state.runningTruck}
          idle={this.state.idleTruck}
          error={this.state.errorTruck}
          stopdata={this.stopedHandler}
          totaldata={this.totalHandler}
          runningdata={this.runningHandler}
          ideldata={this.idelHandler}
          errordata={this.errorHandler}
          component={this.componentDidMount}
          select={this.onSelect}
          dropdownchange={this.dropdownchange}
        />
        <Sidebar data={this.state.data} />
        <div style={{ float: "right", width: "75%" }}>
          {/* <Map data={this.state.data} recogniser={this.state.recogniser} /> */}
          <TabContent activeTab={this.state.activeTab}>
            <TabPane tabId="1">
              <Map data={this.state.data} recogniser={this.state.recogniser} />
            </TabPane>
            <TabPane tabId="2">
              <Map
                data={this.state.runningTruck}
                recogniser={this.state.recogniser}
              />
            </TabPane>
            <TabPane tabId="3">
              <Map
                data={this.state.stoppedTRuck}
                recogniser={this.state.recogniser}
              />
            </TabPane>
            <TabPane tabId="4">
              <Map
                data={this.state.idleTruck}
                recogniser={this.state.recogniser}
              />
            </TabPane>
            <TabPane tabId="5">
              <Map
                data={this.state.errorTruck}
                recogniser={this.state.recogniser}
              />
            </TabPane>
          </TabContent>
        </div>
      </Container>
    );
  }
}
