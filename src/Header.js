import React, { Component } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Tabs from "@material-ui/core/Tabs";

import Tab from "@material-ui/core/Tab";
import { TabPanel } from "@material-ui/lab";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import PhoneIcon from "@material-ui/icons/Phone";
import { withStyles } from "@material-ui/core/styles";
import Multiselect from "multiselect-react-dropdown";
import Select from "react-dropdown-select";
// import GoogleMap from "./Map";
import { Container, Row, Col } from "reactstrap";
import zIndex from "@material-ui/core/styles/zIndex";

const styles = (theme) => ({
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
  },
});

class Header extends Component {
  constructor(props) {
    super(props);

    this.state = {
      value: 0,
      listofid: [],
      selected: [],
      selectedOption: null,
      multi: true,
      disabled: false,
      loading: false,
      contentRenderer: false,
      dropdownRenderer: false,
      inputRenderer: false,
      itemRenderer: false,
      optionRenderer: false,
      noDataRenderer: false,

      clearable: false,
      searchable: true,
      create: false,
      separator: false,
      forceOpen: false,
      handle: true,
      addPlaceholder: "+ click to add",

      color: "blue",
      keepSelectedInList: true,
      closeOnSelect: false,
      dropdownPosition: "bottom",
      direction: "ltr",
      dropdownHeight: "300px",
    };
  }

  handleChange = (selectedOption) => {
    this.setState({ selectedOption });
    console.log(`Option selected:`, selectedOption);
  };

  onSelect = (event) => {
    console.log("onselect", event);
    this.state.selected.push(event);
    // this.props.select(event);
  };

  handleToggle = (event) => {
    console.log("value", event);
    this.setState({
      value: event,
    });
  };

  handleChange = (event) => {
    console.log("value", event);
    this.props.dropdownchange(event);
  };

  totalHandler = () => {
    this.handleToggle(0);
    this.props.totaldata();
  };
  runningHandler = () => {
    this.handleToggle(1);
    this.props.runningdata();
    // this.props.component("runninghandler");
  };
  stoppedHandler = () => {
    this.handleToggle(2);
    this.props.stopdata();
  };
  idelHandler = () => {
    this.handleToggle(3);
    this.props.ideldata();
  };
  errorHandler = () => {
    this.handleToggle(4);
    this.props.errordata();
  };
  render() {
    const { selectedOption } = this.state;
    const { classes } = this.props;
    return (
      <div style={{ height: "87%" }}>
        <Grid container spacing={0}>
          <Grid item xs={3} sm={2}>
            <Paper
              className={classes.paper}
              onClick={this.totalHandler}
              style={{
                background: this.state.value === 0 ? "yellow" : "",
                cursor: "pointer",
                fontWeight: "bold",
              }}
            >
              Total Truck
              <br />
              {this.props.total.length > 0 ? (
                <span>{this.props.total.length}</span>
              ) : (
                0
              )}
            </Paper>
          </Grid>
          <Grid item xs={3} sm={2}>
            <Paper
              className={classes.paper}
              onClick={this.runningHandler}
              style={{
                background: this.state.value === 1 ? "yellow" : "",
                cursor: "pointer",
                fontWeight: "bold",
              }}
            >
              Running Truck
              <br />{" "}
              {this.props.running.length > 0 ? (
                <span>{this.props.running.length}</span>
              ) : (
                0
              )}
            </Paper>
          </Grid>
          <Grid item xs={3} sm={2}>
            <Paper
              className={classes.paper}
              onClick={this.stoppedHandler}
              style={{
                background: this.state.value === 2 ? "yellow" : "",
                cursor: "pointer",
                fontWeight: "bold",
              }}
            >
              Stopped Truck
              <br />{" "}
              {this.props.Stoppped.length > 0 ? (
                <span>{this.props.Stoppped.length}</span>
              ) : (
                0
              )}
            </Paper>
          </Grid>
          <Grid item xs={3} sm={2}>
            <Paper
              className={classes.paper}
              onClick={this.idelHandler}
              style={{
                background: this.state.value === 3 ? "yellow" : "",
                cursor: "pointer",
                fontWeight: "bold",
              }}
            >
              Idel Truck
              <br />{" "}
              {this.props.idle.length > 0 ? (
                <span>{this.props.idle.length}</span>
              ) : (
                0
              )}
            </Paper>
          </Grid>
          <Grid item xs={3} sm={2}>
            <Paper
              className={classes.paper}
              onClick={this.errorHandler}
              style={{
                background: this.state.value === 4 ? "yellow" : "",
                cursor: "pointer",
                fontWeight: "bold",
              }}
            >
              Error Truck
              <br /> {this.props.error.length}
            </Paper>
          </Grid>
          <Grid item xs={3} sm={2}>
            <Paper
              className={classes.paper}
              style={{
                background: this.state.value === 5 ? "yellow" : "",
                cursor: "pointer",
                fontWeight: "bold",
                height: "79px",
              }}
            >
              {/* <input
                onChange={this.handleChange}
                list="browsers"
                name="listofid"
                id="browser"
                className="allselect"
                multiple
              />

              <datalist id="browsers">
                <option>select</option>
                {this.props.data.length > 0
                  ? this.props.data.map((truck) => (
                      <option key={truck.id}>{truck.truckNumber}</option>
                    ))
                  : null}
              </datalist> */}
              <div
                style={{
                  position: "absolute",
                  // backgroundColor: "yellow",
                  zIndex: "1",
                  width: "192px",
                  marginLeft: "1%",
                  // height: "90px",
                }}
              >
                {/* <div style={{ background: "blue" }}>
                  {this.state.selected.length > 0
                    ? this.state.selected.map((data) => <h6> {data}</h6>)
                    : ""}
                </div>

                <input
                  onChange={this.handleChange}
                  list="browsers"
                  name="listofid"
                  id="browser"
                  multiple
                />

                <datalist id="browsers">
                  <option>select</option>
                  {this.props.data.length > 0
                    ? this.props.data.map((truck) => (
                        <option key={truck.id}>{truck.truckNumber}</option>
                      ))
                    : null}
                </datalist> */}
                <div style={{ maxWidth: "350px", margin: "0 auto" }}>
                  <Select
                    placeholder="Search"
                    addPlaceholder={this.state.addPlaceholder}
                    color={this.state.color}
                    disabled={this.state.disabled}
                    loading={this.state.loading}
                    searchBy="truckNumber"
                    separator={this.state.separator}
                    clearable={this.state.clearable}
                    searchable={this.state.searchable}
                    create={this.state.create}
                    keepOpen={this.state.forceOpen}
                    dropdownHandle={this.state.handle}
                    dropdownHeight={this.state.dropdownHeight}
                    direction={this.state.direction}
                    multi={this.state.multi}
                    dropdownGap={0}
                    // values={[
                    //   this.props.data.find(
                    //     (opt) => opt.username === "Delphine"
                    //   ),
                    // ]}
                    labelField="truckNumber"
                    valueField={"id"}
                    options={this.props.total}
                    dropdownGap={5}
                    keepSelectedInList="true"
                    onChange={(values) => this.handleChange(values)}
                    noDataLabel="No matches found"
                    closeOnSelect={this.state.closeOnSelect}
                    dropdownPosition={this.state.dropdownPosition}
                  />
                </div>
              </div>
            </Paper>
          </Grid>
        </Grid>
        {/* <GoogleMap /> */}
      </div>
    );
  }
}

export default withStyles(styles)(Header);
