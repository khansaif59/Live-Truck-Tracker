import React, { Component } from "react";
import axios from "axios";
import Header from "./Header";
// import Sidebar from "./Sidebar";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Divider from "@material-ui/core/Divider";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import Avatar from "@material-ui/core/Avatar";
import Typography from "@material-ui/core/Typography";
import Empty from "./Assets/cloud-computing.png";
export default class Sidebar extends Component {
  constructor(props) {
    super(props);

    this.state = { currentdate: "" };
  }

  getTimeDifference = (date1, date2) => {
    // var date2 = new Date(date2);
    var difference = date1.getTime() - date2.getTime();

    var daysDifference = Math.floor(difference / 1000 / 60 / 60 / 24);
    difference -= daysDifference * 1000 * 60 * 60 * 24;

    var hoursDifference = Math.floor(difference / 1000 / 60 / 60);
    difference -= hoursDifference * 1000 * 60 * 60;

    var minutesDifference = Math.floor(difference / 1000 / 60);
    difference -= minutesDifference * 1000 * 60;

    var secondsDifference = Math.floor(difference / 1000);
    // return hoursDifference;
    if (daysDifference > 0) {
      // console.log("checking data=>", data.value);
      return daysDifference + "d";
    } else if (hoursDifference > 0) {
      // console.log("checking data=>", data);
      return hoursDifference + "h";
    } else {
      // console.log("checking data=>", data);
      return minutesDifference + "m";
    }
  };

  render() {
    return (
      <div
        style={{
          float: "left",
          width: "25%",
          // maxWidth: 360,
          // backgroundColor: theme.palette.background.paper,
          position: "absolute",

          overflow: "scroll",
          // maxHeight: 530,
          // height: "89.4%",
          height: "87.2vh",
          margin: 0,
        }}
      >
        <List>
          {this.props.data.length > 0 ? (
            this.props.data.map((data) => (
              <div style={{ borderBottom: "1px solid black" }}>
                {" "}
                <ListItem alignItems="flex-start" key={data.id}>
                  <ListItemText
                    primary={
                      <div>
                        <div
                          style={{
                            float: "right",
                            fontSize: "12px",
                            fontWeight: "bold",
                          }}
                        >
                          {this.getTimeDifference(
                            new Date(),
                            new Date(data.lastWaypoint.createTime)
                          )}
                        </div>

                        {data.truckNumber}
                      </div>
                    }
                    secondary={
                      <React.Fragment>
                        <Typography
                          component="span"
                          variant="body2"
                          color="textPrimary"
                        >
                          {data.lastRunningState.truckRunningState === 0 ? (
                            <span>
                              Stopped since Last{" "}
                              {this.getTimeDifference(
                                new Date(),
                                new Date(data.lastRunningState.stopStartTime)
                              )}
                              {/* {data.lastRunningState.stopStartTime} */}
                              &nbsp;&nbsp;
                            </span>
                          ) : (
                            <span>
                              Running since Last{" "}
                              {/* {data.lastRunningState.stopStartTime} */}
                              {this.getTimeDifference(
                                new Date(),
                                new Date(data.lastRunningState.stopStartTime)
                              )}
                              {/* &nbsp;&nbsp;
                              {data.createTime} */}
                            </span>
                          )}
                        </Typography>
                      </React.Fragment>
                    }
                  />
                </ListItem>
              </div>
            ))
          ) : (
            <div style={{ display: "block", marginLeft: 0, marginLeft: 0 }}>
              <img
                src={Empty}
                style={{ width: "57%", marginTop: "50%", marginLeft: "20%" }}
              />
            </div>
          )}
        </List>
      </div>
    );
  }
}
