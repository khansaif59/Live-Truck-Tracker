import React, { Component } from "react";
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker,
} from "react-google-maps";
import "bootstrap/dist/css/bootstrap.min.css";
import { Container } from "reactstrap";

import red from "./Assets/red.png";
import blue from "./Assets/blue.png";
import green from "./Assets/green.png";
import yellow from "./Assets/yellow.png";

const MyMapComponent = withScriptjs(
  withGoogleMap((props) => (
    <GoogleMap defaultZoom={8} defaultCenter={{ lat: 30.7333, lng: 76.7794 }}>
      {props.data.length > 0
        ? props.data.map((e, index) => (
            <Marker
              icon={{
                url:
                  e.lastRunningState.truckRunningState === 1
                    ? green
                    : e.lastRunningState.truckRunningState === 0 &&
                      e.lastWaypoint.ignitionOn === true
                    ? yellow
                    : e.lastRunningState.truckRunningState === 0
                    ? blue
                    : e.lastWaypoint.ignitionOn === false
                    ? yellow
                    : e.breakdown === true
                    ? red
                    : "",

                anchor: new window.google.maps.Point(10, 10),
                scaledSize: new window.google.maps.Size(40, 40),
              }}
              position={{
                lat: e.lastWaypoint.lat,
                lng: e.lastWaypoint.lng,
              }}
            />
          ))
        : null}
    </GoogleMap>
  ))
);

// const MyMapComponent = withScriptjs(
//   withGoogleMap((props) => {
//     // console.log("shahid=================================>>>", props.data);
//     <GoogleMap defaultZoom={8} defaultCenter={{ lat: 30.7333, lng: 76.7794 }}>
//       //{" "}
//       {/* {props.data.length > 0
//         ? props.data.map((e) => (
//             <Marker position={{ lat: 30.7333, lng: 76.7744 }} />
//           ))
//         : null}
//       //{" "} */}
//     </GoogleMap>;
//   })
// );

class Map extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
    };
  }

  // componentDidUpdate = (nextProps) => {
  //   console.log("should component calling", nextProps);
  //   console.log("should component props", this.props.data);
  //   if (nextProps !== this.props.value) {
  //     this.setState({
  //       data: this.props.data,
  //     });
  //     return true;
  //   } else {
  //     return false;
  //   }
  // };

  render() {
    console.log("component rerender");
    return (
      <Container
        fluid
        style={{
          width: "100%",
          maxWidth: "none",
          paddingLeft: 0,
          paddingRight: 0,
        }}
      >
        <div>
          <MyMapComponent
            isMarkerShown={true}
            // data={this.props.data.length > 0 ? this.props.data : []}
            data={this.props.data}
            googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyAN7mwyR_B3NDDLnuwkLds1RU9IUSBklc8&v=3.exp&libraries=geometry,drawing,places"
            loadingElement={<div style={{ height: `100%` }} />}
            // containerElement={<div style={{ height: `545px` }} />}
            containerElement={
              <div style={{ height: "87.2vh", maxWidth: "100vw" }} />
            }
            mapElement={<div style={{ height: `100%` }} />}
          ></MyMapComponent>
        </div>
      </Container>
    );
  }
}

export default Map;
